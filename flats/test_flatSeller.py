from unittest import TestCase
from flats.flatSeller import FlatSeller


class TestFlatSeller(TestCase):

    def test_price_flat_linear(self):
        flat_seller = FlatSeller()
        flat_seller.train_with_data_set("../flats_to_sell.csv")
        ref_value = flat_seller.price_flat_linear(90, 4, True, 3)
        self.assertLess(flat_seller.price_flat_linear(90, 4, False, 3),
                        ref_value)
        self.assertLess(flat_seller.price_flat_linear(55, 4, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 3),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 9, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 1),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 1, True, 9),
                        ref_value)

    def test_price_flat_square(self):
        flat_seller = FlatSeller()
        flat_seller.train_with_data_set("../flats_to_sell.csv")
        ref_value = flat_seller.price_flat_linear(90, 4, True, 3)
        self.assertLess(flat_seller.price_flat_linear(90, 4, False, 3),
                        ref_value)
        self.assertLess(flat_seller.price_flat_linear(55, 4, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 3),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 9, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 1),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 1, True, 9),
                        ref_value)

    def test_price_flat(self):
        flat_seller = FlatSeller()
        flat_seller.train_with_data_set("../flats_to_sell.csv")
        ref_value = flat_seller.price_flat_linear(90, 4, True, 3)
        self.assertLess(flat_seller.price_flat_linear(90, 4, False, 3),
                        ref_value)
        self.assertLess(flat_seller.price_flat_linear(55, 4, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 3),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 9, True, 3),
                        ref_value)
        self.assertGreater(flat_seller.price_flat_linear(90, 1, True, 1),
                           ref_value)
        self.assertLess(flat_seller.price_flat_linear(90, 1, True, 9),
                        ref_value)

