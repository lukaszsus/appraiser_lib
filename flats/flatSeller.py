import pandas as pd
import numpy as np


class FlatSeller:
    """Class pricing flats for selling."""

    def __init__(self):
        """Constructor."""
        self.theta_linear = [1] * 5
        self.theta_square = [1] * 8
        self.X_linear = None
        self.X_square = None
        self.Y = None
        self.price = None

    def create_training_set(self, data_set):
        tmp_X = data_set[["area", "floor", "cellar", "distance"]]
        tmp_X["area2"] = tmp_X["area"] ** 2
        tmp_X["floor2"] = tmp_X["floor"] ** 2
        tmp_X["distance2"] = tmp_X["distance"] ** 2
        tmp_X["bias"] = [1] * tmp_X["area"].count()
        i = 1

        X = pd.DataFrame()
        for column in tmp_X.columns.values:
            if column == "bias":
                X[0] = tmp_X[column]
            else:
                # because there are some bool type columns
                X[i] = list(map(int, tmp_X[column]))
                i += 1

        # Columns for linear model.
        columns_linear = [i - 1]
        columns_linear += range(0, 4)

        # Columns for square model.
        columns_square = [i - 1]
        columns_square += range(0, 7)

        # Preparing data set for linear model.
        self.X_linear = np.array(X)
        self.X_linear = np.matrix(self.X_linear[:, columns_linear])

        self.X_square = np.array(X)
        self.X_square = np.matrix(self.X_square[:, columns_square])

        self.Y = np.matrix(data_set[["price"]])

    def count_std_dev(self, theta, type):
        if type == "linear":
            outcomes = self.X_linear * theta
        if type == "square":
            outcomes = self.X_square * theta
        diff = self.Y - outcomes
        std_dev = np.power(diff, 2)
        std_dev /= len(std_dev)
        std_dev = sum(std_dev)
        return std_dev.tolist()

    def train_linear_model(self):
        L = np.identity(5)
        L[0][0] = 0

        std_dev = []

        for i in range(0, 21):
            lambda_factor = i * 0.05
            theta_linear = (
                                   self.X_linear.getT() *
                                   self.X_linear + lambda_factor * L
                           ).getI() * self.X_linear.getT() * self.Y
            std_dev += [self.count_std_dev(theta_linear, "linear")]

        index = std_dev.index(min(std_dev))
        lambda_factor = index * 0.05

        self.theta_linear = (
                                    self.X_linear.getT() *
                                    self.X_linear + lambda_factor * L
                            ).getI() * self.X_linear.getT() * self.Y

        return min(std_dev)

    def train_square_model(self):
        L = np.identity(8)
        L[0][0] = 0

        std_dev = []

        for i in range(0, 21):
            lambda_factor = i * 0.05
            theta_square = (
                                   self.X_square.getT() *
                                   self.X_square + lambda_factor * L
                           ).getI() * self.X_square.getT() * self.Y
            std_dev += [self.count_std_dev(theta_square, "square")]

        index = std_dev.index(min(std_dev))
        lambda_factor = index * 0.05

        self.theta_square = (
                                    self.X_square.getT() *
                                    self.X_square + lambda_factor * L
                            ).getI() * self.X_square.getT() * self.Y

        return min(std_dev)

    def train_with_data_set(self, file_name):
        data_set = pd.read_csv(file_name, index_col=0)
        self.create_training_set(data_set)

        linear_dev = self.train_linear_model()
        square_dev = self.train_square_model()

        if linear_dev <= square_dev:
            self.price = self.price_flat_linear
        else:
            self.price = self.price_flat_square

    def price_flat_linear(self, area, floor, cellar, distance_to_centre):
        """Method which prices an unknown flat counted by linear model."""
        return float(
            self.theta_linear[0]
            + self.theta_linear[1] * float(area)
            + self.theta_linear[2] * int(floor)
            + self.theta_linear[3] * int(cellar)
            + self.theta_linear[4] * float(distance_to_centre))

    def price_flat_square(self, area, floor, cellar, distance_to_centre):
        """Method which prices an unknown flat counted by square model."""
        return float(
            self.theta_square[0]
            + self.theta_square[1] * float(area)
            + self.theta_square[2] * int(floor)
            + self.theta_square[3] * int(cellar)
            + self.theta_square[4] * float(distance_to_centre)
            + self.theta_square[5] * float(area) ** 2
            + self.theta_square[6] * int(floor) ** 2
            + self.theta_square[7] * float(distance_to_centre) ** 2)

    def price_flat(self, area, floor, cellar, distance_to_centre):
        return self.price(area, floor, cellar, distance_to_centre)
