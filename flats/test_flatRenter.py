from unittest import TestCase
from flats.flatRenter import FlatRenter


class TestFlatRenter(TestCase):

    def test_price_flat_linear(self):
        flat_renter = FlatRenter()
        flat_renter.train_with_data_set("../flats_to_rent.csv")
        ref_value = flat_renter.price_flat_linear(90, 4, True, 3, 4, True)
        self.assertLess(flat_renter.price_flat_linear(90, 4, False, 3, 4, True),
                        ref_value)
        self.assertLess(flat_renter.price_flat_linear(55, 4, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_linear(90, 1, True, 3, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price_flat_linear(90, 9, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_linear(90, 1, True, 1, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price_flat_linear(90, 1, True, 9, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_linear(90, 4, True, 3, 2, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_linear(90, 4, True, 3, 3, True),
                           ref_value)
        self.assertLess(flat_renter.price_flat_linear(90, 4, True, 3, 5, True),
                        ref_value)
        self.assertLess(flat_renter.price_flat_linear(90, 4, True, 3, 4, False),
                        ref_value)

    def test_price_flat_square(self):
        flat_renter = FlatRenter()
        flat_renter.train_with_data_set("../flats_to_rent.csv")
        ref_value = flat_renter.price_flat_square(90, 4, True, 3, 4, True)
        print(ref_value)
        self.assertLess(flat_renter.price_flat_square(90, 4, False, 3, 4, True),
                        ref_value)
        self.assertLess(flat_renter.price_flat_square(55, 4, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_square(90, 1, True, 3, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price_flat_square(90, 9, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price_flat_square(90, 1, True, 1, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price_flat_square(90, 4, True, 9, 4, True),
                        ref_value)
        self.assertNotEqual(flat_renter.price_flat_square(90, 4, True, 3, 2, True),
                        ref_value)
        self.assertNotEqual(flat_renter.price_flat_square(90, 4, True, 3, 3, True),
                           ref_value)
        self.assertNotEqual(flat_renter.price_flat_square(90, 4, True, 3, 5, True),
                        ref_value)
        self.assertLess(flat_renter.price_flat_square(90, 4, True, 3, 4, False),
                        ref_value)

    def test_price_flat(self):
        flat_renter = FlatRenter()
        flat_renter.train_with_data_set("../flats_to_rent.csv")
        ref_value = flat_renter.price(90, 4, True, 3, 4, True)
        self.assertLess(flat_renter.price(90, 4, False, 3, 4, True),
                        ref_value)
        self.assertLess(flat_renter.price(55, 4, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price(90, 1, True, 3, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price(90, 9, True, 3, 4, True),
                        ref_value)
        self.assertGreater(flat_renter.price(90, 1, True, 1, 4, True),
                           ref_value)
        self.assertLess(flat_renter.price(90, 4, True, 9, 4, True),
                        ref_value)
        self.assertNotEqual(flat_renter.price(90, 4, True, 3, 2, True),
                        ref_value)
        self.assertNotEqual(flat_renter.price(90, 4, True, 3, 3, True),
                           ref_value)
        self.assertNotEqual(flat_renter.price(90, 4, True, 3, 5, True),
                        ref_value)
        self.assertLess(flat_renter.price(90, 4, True, 3, 4, False),
                        ref_value)
