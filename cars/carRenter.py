class CarRenter:
    """Class pricing cars for renting."""
    def __init__(self):
        self.is_diesel_factor = 0.8
        self.is_not_diesel_factor = 0.9
        self.started_price = 400

    def price_linear(self, years, engine_capacity, is_diesel, number_of_days):
        factor = is_diesel and self.is_diesel_factor or not is_diesel and self.is_not_diesel_factor
        factor -= (engine_capacity / 10)
        number_of_days_factor = 1 - (number_of_days * 0.05) ** 2
        return self.started_price / (factor * years) * number_of_days_factor

    def price(self, years, engine_capacity, is_diesel, number_of_days):
        return self.price_linear(years, engine_capacity, is_diesel, number_of_days)
