class CarSeller:
    """Class pricing cars for selling."""
    def __init__(self):
        self.is_diesel_factor = 0.8
        self.is_not_diesel_factor = 0.9

    def price_linear(self, started_price, years, engine_capacity, is_diesel):
        factor = is_diesel and self.is_diesel_factor or not is_diesel and self.is_not_diesel_factor
        factor -= (engine_capacity / 10)
        return started_price / (factor * years)

    def price(self, started_price, years, engine_capacity, is_diesel):
        return self.price_linear(started_price, years, engine_capacity, is_diesel)
